.model small
.code
org 100h
start:
       jmp mulai
nama       db 13,10,'Nama Anda   :$'
hp         db 13,10,'No. HP/Telp :$'
kode       db 13,10,'kode poduk yang anda pilih/inginkan :$'
lanjut   db 13,10,'LANJUT Tekan y untuk lanjut >>>>>>>>>>>>> $'
tampung_nama  db 30,?,30 dup(?)
tampung_hp    db 13,?,13 dup(?)
tampung_produk  db 13,?,13 dup(?)

a db 01
b db 02
c db 03
d db 04
e db 05
f db 06
g db 07
h db 08
i db 09
j dw 15

daftar db 13,10,'+-------------------------------------+'
     db 13,10,'          DAFTAR PRODUK SKINCARE     |'
     db 13,10,'+---+--------------+------+-------------+'
     db 13,10,'|No|Jenis             |Paket   | Harga   |'
     db 13,10,'+---+--------------+------+-------------+'
     db 13,10,'|01| SOME BY MI       | 1      | 300.000 |'
     db 13,10,'+---+--------------+------+-------------+'
     db 13,10,'|02| MS GLOW          | 1      | 300.000 |'
     db 13,10,'+---+--------------+------+-------------+'
     db 13,10,'|03| CENTELLA BRIGHTENING  | 1  |  769.000  |'
     db 13,10,'+---+--------------+------+-------------+'
     db 13,10,'|04| HANASUI          |  1   |  80.000   |'
     db 13,10,'+---+--------------+------+-------------+'
     db 13,10,'|05|  MORESKIN        |  1   |  350.000  |','$'


daftar2 db 13,10,'+---+-----------+------+-------------+'
     db 13,10,'|No|Jenis              |Paket    |Harga   |'
     db 13,10,'+---+------------+----+--------------+'
     db 13,10,'|06| BENINGS          | 1       | 350.000 |'
     db 13,10,'+---+------------+----+--------------+'
     db 13,10,'|07| SCARLET          | 1       | 208.000 |'
     db 13,10,'+---+------------+----+--------------+'
     db 13,10,'|08| EMINA            | 1       | 87.000  |'
     db 13,10,'+---+------------+----+--------------+'
     db 13,10,'|09| COSRX            | 1       | 249.000 |'
     db 13,10,'+---+------------+----+--------------+'
     db 13,10,'|10| SOMETHINC        | 1       | 97.000  |','$'


daftar3 db 13,10,'+---+-----------+-----+--------------+'
     db 13,10,'|No|Jenis              |Paket    | Harga   |'
     db 13,10,'+---+-------------+----+--------------+'
     db 13,10,'|11| GARNIER PAKET LIGHT COMPLIT  | 1 |  141.000  |'
     db 13,10,'+---+-------------+----+--------------+'
     db 13,10,'|12| GARNIER PAKET SAKURA WHITE   | 1 |  141.000  |'
     db 13,10,'+---+-------------+----+--------------+'
     db 13,10,'|13| AZARINE ACNE  | 1  |  138.000   |'
     db 13,10,'+---+-------------+----+--------------+'
     db 13,10,'|14| AZARINE C WHITE | 1 | 230.000   |'
     db 13,10,'+---+-------------+----+--------------+'
     db 13,10,'|15| SAFI (KULIT KERING/NORMAL)  | 1  |  220.000   |','$'


daftar4 db 13,10,'+---+--------+----+------------------+'
     db 13,10,'|No|Jenis               |Paket     | Harga   |'
     db 13,10,'+---+------------+----+---------------+'
     db 13,10,'|16| OSHIN BEAUTY        | 1        | 300.000 |'
     db 13,10,'+---+------------+----+---------------+'
     db 13,10,'|17| WARDAH LIGHTENING   | 1        | 148.000 |'
     db 13,10,'+---+------------+----+---------------+'
     db 13,10,'|18| WARDAH CRYSTAL SECRET  | 1     | 313.000 |'
     db 13,10,'+---+------------+----+---------------+'
     db 13,10,'|19| SKINSENA PAKET A    | 1        | 119.000 |'
     db 13,10,'+---+------------+----+---------------+'
     db 13,10,'|20| SKINSENA PAKET B    | 1        | 157.000 |'
     db 13,10,'+---+------------+----+---------------+'
     db 13,10,'TENTUKAN SKINCARE PILIHAN ANDA SESUAI KODE ATAU NAMA PRODUK  |'
     db 13,10,'+---+------------+----+---------------+','$'


error  db 13,10,'KODE YANG ANDA MASUKKAN SALAH $'
pilih_mtr  db 13,10,'SIAHKAN MASUKKAN KODE ATAU NAMA PRODUK YANG ANDA PILIH $'
success  db 13,10,'SEAMAT ANDA BERHASIL $'

   mulai:
   mov ah,09h
   lea dx,nama
   int 21h
   mov ah,0ah
   lea dx,tampung_nama
   int 21h
   push dx

   
   mov ah,09h
   lea dx,hp
   int 21h
   mov ah,0ah
   lea dx,tampung_hp
   int 21h
   push dx


   mov ah,09h
   mov dx,offset daftar
   int 21h
   mov ah,09h
   mov dx,offset lanjut
   int 21h
   mov ah,01h
   int 21h
   cmp al,'y'
   je page2
   jne error_msg

page2:
   mov ah,09h
   mov dx,offset daftar2
   int 21h
   mov ah,09h
   mov dx,offset lanjut
   int 21h
   mov ah,01h
   int 21h
   cmp al,'y'
   je page3
   jne error_msg

page3:
   mov ah,09h
   mov dx,offset daftar3
   int 21h
   mov ah,09h
   mov dx,offset lanjut
   int 21h
   mov ah,01h
   int 21h
   cmp al,'y'
   je page4
   jne error_msg

page4:
   mov ah,09h
   mov dx,offset daftar4
   int 21h
   mov ah,09h
   mov dx,offset lanjut
   int 21h
   mov ah,01h
   int 21h
   cmp al,'y'
 
mov ah,09h
   lea dx,kode
   int 21h
   mov ah,01h
   int 21h
   cmp al,'y'
   jmp proses
   jne error_msg

error_msg:
    mov ah,09h
    mov dx,offset error
    int 21h
    int 20h

proses:
    mov ah,09h
    mov dx,offset pilih_mtr
    int 21h

    mov ah,1
    int 21h
    mov bh,al
    mov al,1
    int 21h
    mov bl,al
    cmp bh,'0'
    cmp bl,'1'
    je hasil1

    cmp bh,'0'
    cmp bl,'2'
    je hasil2

    cmp bh,'0'
    cmp bl,'3'
    je hasil3

    cmp bh,'0'
    cmp bl,'4'
    je hasil4

    cmp bh,'0'
    cmp bl,'5'
    je hasil5

    cmp bh,'0'
    cmp bl,'6'
    je hasil6
 
    cmp bh,'0'
    cmp bl,'7'
    je hasil7

    cmp bh,'0'
    cmp bl,'8'
    je hasil8

    cmp bh,'0'
    cmp bl,'9'
    je hasil9

    ;cmp bh,'1'
    ;cmp bl,'0'
    je hasil10

    ;cmp bh,'1'
    ;cmp bl,'1'
    je hasil11

    ;cmp bh,'1'
    ;cmp bl,'2'
    je hasil12
  
    ;cmp bh,'1'
    ;cmp bl,'3'
    je hasil13
    
    ;cmp bh,'1'
    ;cmp bl,'4'
    je hasil14

    ;cmp bh,'1'
    ;cmp bl,'5'
    je hasil15

    ;cmp bh,'1'
    ;cmp bl,'6'
    je hasil16

    ;cmp bh,'1'
    ;cmp bl,'7'
    je hasil17

    ;cmp bh,'1'
    ;cmp bl,'8'
    je hasil18

    ;cmp bh,'1'
    ;cmp bl,'9'
    je hasil19

    ;cmp bh,'2'
    ;cmp bl,'0'
    je hasil20

    jne error_msg



hasil1:
     mov ah,09h
     lea dx,teks1
     int 21h
     int 20h

hasil2:
     mov ah,09h
     lea dx,teks2
     int 21h
     int 20h

hasil3:
     mov ah,09h
     lea dx,teks3
     int 21h
     int 20h

hasil4:
     mov ah,09h
     lea dx,teks4
     int 21h
     int 20h

hasil5:
     mov ah,09h
     lea dx,teks5
     int 21h
     int 20h

hasil6:
     mov ah,09h
     lea dx,teks6
     int 21h
     int 20h

hasil7:
     mov ah,09h
     lea dx,teks7
     int 21h
     int 20h

hasil8:
     mov ah,09h
     lea dx,teks8
     int 21h
     int 20h

hasil9:
     mov ah,09h
     lea dx,teks9
     int 21h
     int 20h

hasil10:
     mov ah,09h
     lea dx,teks10
     int 21h
     int 20h

hasil11:
     mov ah,09h
     lea dx,teks11
     int 21h
     int 20h

hasil12:
     mov ah,09h
     lea dx,teks12
     int 21h
     int 20h

hasil13:
     mov ah,09h
     lea dx,teks13
     int 21h
     int 20h

hasil14:
     mov ah,09h
     lea dx,teks14
     int 21h
     int 20h

hasil15:
     mov ah,09h
     lea dx,teks15
     int 21h
     int 20h

hasil16:
     mov ah,09h
     lea dx,teks16
     int 21h
     int 20h

hasil17:
     mov ah,09h
     lea dx,teks17
     int 21h
     int 20h

hasil18:
     mov ah,09h
     lea dx,teks18
     int 21h
     int 20h

hasil19:
     mov ah,09h
     lea dx,teks19
     int 21h
     int 20h

hasil20:
     mov ah,09h
     lea dx,teks20
     int 21h
     int 20h

;------------------------------------------------------

teks1 db 13,10,'ANDA MEMILIH PRODUK SOME BY MI'
     db 13,10,'PENJUALAN PRODUK PERPAKET'
     db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 300.000'
     db 13,10,'Terima Kasih $'

teks2 db 13,10,'ANDA MEMILIH PRODUK MS GLOW'
     db 13,10,'PENJUALAN PRODUK PERPAKET'
     db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 300.000'
     db 13,10,'Terima Kasih $'
  
teks3 db 13,10,'ANDA MEMILIH PRODUK CENTELLA BRIGHTENING'
     db 13,10,'PENJUALAN PRODUK PERPAKET'
     db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 769.000'
     db 13,10,'Terima Kasih $'

teks4 db 13,10,'ANDA MEMILIH PRODUK HANASUI'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 80.000'
      db 13,10,'Terima Kasih $'

teks5 db 13,10,'ANDA MEMILIH PRODUK MORESKIN'
     db 13,10,'PENJUALAN PRODUK PERPAKET'
     db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 350.000'
     db 13,10,'Terima Kasih $'

teks6 db 13,10,'ANDA MEMILIH PRODUK BENINGS'
     db 13,10,'PENJUALAN PRODUK PERPAKET'
     db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 350.000'
     db 13,10,'Terima Kasih $'

teks7 db 13,10,'ANDA MEMILIH PRODUK SCARLET'
     db 13,10,'PENJUALAN PRODUK PERPAKET'
     db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 208.000'
     db 13,10,'Terima Kasih $'

teks8 db 13,10,'ANDA MEMILIH PRODUK EMINA'
     db 13,10,'PENJUALAN PRODUK PERPAKET'
     db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 87.000'
     db 13,10,'Terima Kasih $'

teks9 db 13,10,'ANDA MEMILIH PRODUK COSRX'
     db 13,10,'PENJUALAN PRODUK PERPAKET'
     db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 249.000'
     db 13,10,'Terima Kasih $'

teks10 db 13,10,'ANDA MEMILIH PRODUK SOMETHINC'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 97.000'
      db 13,10,'Terima Kasih $'                                                   
      

teks11 db 13,10,'ANDA MEMILIH PRODUK GARNIER PAKET LIGHT COMPLIT'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 141.000'
      db 13,10,'Terima Kasih $'

teks12 db 13,10,'ANDA MEMILIH PRODUK GARNIER PAKET SAKURA WHITE'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 141.000'
      db 13,10,'Terima Kasih $'

teks13 db 13,10,'ANDA MEMILIH PRODUK AZARINE ACNE'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 138.000'
      db 13,10,'Terima Kasih $'

teks14 db 13,10,'ANDA MEMILIH PRODUK AZARINE C WHITE'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 230.000'
      db 13,10,'Terima Kasih $'

teks15 db 13,10,'ANDA MEMILIH PRODUK SAFI'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 220.000'
      db 13,10,'Terima Kasih $'

teks16 db 13,10,'ANDA MEMILIH PRODUK OSHIN BEAUTY'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 300.000'
      db 13,10,'Terima Kasih $'

teks17 db 13,10,'ANDA MEMILIH PRODUK WARDAH LIGHTENING'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 148.000'
      db 13,10,'Terima Kasih $'
            
teks18 db 13,10,'ANDA MEMILIH PRODUK WARDAH CRYSTAL SECRET'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 313.000'
      db 13,10,'Terima Kasih $'

teks19 db 13,10,'ANDA MEMILIH PRODUK SKINSENA PAKET A'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 119.000'
      db 13,10,'Terima Kasih $'

teks20 db 13,10,'ANDA MEMILIH PRODUK SKINSENA PAKET B'
      db 13,10,'PENJUALAN PRODUK PERPAKET'
      db 13,10,'BIAYA YANG HARUS ANDA BAYAR SEJUMLAH : Rp. 157.000'
      db 13,10,'Terima Kasih $'

end start